use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::fs;
use std::io::Result;
use std::process::Command;

/// Donation platform regex string
const LIBERAPAY_REGEX: &str = r"(https?://)?(www.)?liberapay.com/[[:alnum:]]+(/donate)?";
const PATREON_REGEX: &str = r"(https?://)?(www.)?patreon.com/[[:alnum:]]+";

/// Struct to store package information
#[derive(Debug)]
struct PackageInformation {
    name: String,
    _url: String,
    readme_path: Option<String>,
}

impl PackageInformation {
    /// Search a file for a regex pattern
    fn search_in_readme(&self, regexs: &[&Regex]) -> Result<Option<Vec<String>>> {
        Ok(match &self.readme_path {
            None => None,
            Some(path) => {
                let contents = fs::read_to_string(path)?;
                let urls: Vec<String> = regexs
                    .iter()
                    .flat_map(|regex| regex.find_iter(&contents))
                    .map(|m| m.as_str().to_string())
                    .unique()
                    .collect();
                if urls.is_empty() {
                    None
                } else {
                    Some(urls)
                }
            }
        })
    }

    /// Find all donation URLs in a given package's README
    fn find_donation_urls(&self) -> Result<Option<Vec<String>>> {
        lazy_static! {
            // Could use RegexSet here, but RegexSet does not find
            // TODO: The laziness probably doesn't apply anymore now that it is
            // bound to a struct.
            static ref LIBERAPAY_RE: Regex = Regex::new(LIBERAPAY_REGEX).unwrap();
            static ref PATREON_RE: Regex = Regex::new(PATREON_REGEX).unwrap();
        }
        self.search_in_readme(&[&LIBERAPAY_RE, &PATREON_RE])
    }
}

#[derive(Clone)]
/// Struct for Package Managers
/// Fields:
/// - `version_command`: Command to run to check the version
/// - `version_regex`: Excpected output from version check
/// - `list_installed_command`: Command to list every installed package
/// - `list_readmes_command`: Command to list all READMEs
///
/// `version_command` and `version_regex` are used together to determine whether
/// the package manager is installed on the system.
struct PackageManager<'a> {
    version_command: &'a str,
    version_regex: &'a str,
    list_installed_command: &'a str,
    list_readmes_command: &'a str,
}

impl<'a> PackageManager<'a> {
    /// Check if this package manager is installed
    fn is_installed(&self) -> Result<bool> {
        let out = self.execute(self.version_command)?;
        let regex = Regex::new(self.version_regex).unwrap();
        Ok(regex.is_match(&out))
    }

    /// Get a list of all installed packages.
    fn get_list_of_installed_packages(&self) -> Result<Vec<PackageInformation>> {
        let out = self.execute(self.list_installed_command)?;
        let lines: Vec<_> = out
            .lines()
            .map(String::from)
            .tuples()
            .map(|(name, url)| PackageInformation {
                _url: url,
                readme_path: None,
                name,
            })
            .collect();
        Ok(lines)
    }

    /// Get all READMEs from installed packages
    fn get_all_readmes(&self) -> Result<Vec<(String, String)>> {
        let out = self.execute(self.list_readmes_command)?;
        Ok(out
            .lines()
            .map(str::split_ascii_whitespace)
            .map(|mut strings| {
                (
                    strings.next().unwrap().to_string(),
                    strings.last().unwrap().to_string(),
                )
            })
            .collect())
    }

    /// Execute code and return the stdout as a string
    fn execute(&self, command: &'a str) -> Result<String> {
        Ok(String::from_utf8(Command::new("sh").arg("-c").arg(command).output()?.stdout).unwrap())
    }
}

/// RPM Package Manager
const RPM: PackageManager = PackageManager {
    version_command: "rpm --version",
    version_regex: "RPM version ",
    list_installed_command: "rpm -qa --queryformat '%{NAME}\n%{URL}\n'",
    list_readmes_command: "rpm -qa --filesbypkg |grep README",
};

/// Pacman Package Manager
const PACMAN: PackageManager = PackageManager {
    version_command: "pacman --version",
    version_regex: "Pacman v",
    list_installed_command: r"pacman -Qi| awk '/^(URL|Name)/ {print $NF}'",
    list_readmes_command: r"pacman -Ql|awk '/README/'",
};

const DPKG: PackageManager = PackageManager {
    version_command: "dpkg --version",
    version_regex: "Debian 'dpkg' package management program version ",
    list_installed_command: r#"dpkg-query -f '${db:Status-Abbrev} ${Package} ${Homepage}\n' -W '*'|grep ii| awk '{print $2"\n"$3}'"#,
    list_readmes_command: r#"for p in `dpkg-query -f '${db:Status-Abbrev} ${Package}\n' -W '*'|grep ii| awk '{print $2 " "}'`; do for f in `dpkg-query -L $p`; do echo -n $p" "; echo $f; done; done |grep README"#,
};

/// All Package Managers
const PACKAGE_MANAGERS: [PackageManager; 3] = [RPM, PACMAN, DPKG];

/// Function to detect the installed package manager.
fn detect_package_managers<'a>() -> Vec<PackageManager<'a>> {
    PACKAGE_MANAGERS
        .into_iter()
        .filter(|pm| pm.is_installed().unwrap())
        .collect()
}

/// Find a package manager by name
fn match_package_managers<'a>(name: &str) -> Option<PackageManager<'a>> {
    match name {
        "rpm" => Some(RPM),
        "pacman" => Some(PACMAN),
        "dpkg" => Some(DPKG),
        _ => None,
    }
}

/// Create a hashmap containing packages and add their READMEs
fn create_store(
    packages: Vec<PackageInformation>,
    readmes: Vec<(String, String)>,
) -> HashMap<String, PackageInformation> {
    let mut store: HashMap<_, _> = packages.into_iter().map(|p| (p.name.clone(), p)).collect();
    for r in readmes {
        let p = store.get_mut(&r.0);
        match p {
            Some(pkg) => pkg.readme_path = Some(r.1),
            None => println!("Could not find package {}", r.0),
        }
    }
    store
}

use clap::Parser;
/// Command line arguments
#[derive(Parser)]
#[command(author, version, about)]
struct Args {
    #[arg(short, long, value_delimiter = ',')]
    packagemanagers: Vec<String>,
    #[arg(short, long)]
    exceptions: bool,
    #[arg(short, long, default_value_t = 1000)]
    notifyincrements: usize,
    #[arg(short, long)]
    minimal: bool,
}

fn main() -> Result<()> {
    let args = Args::parse();
    // Find the installed package managers
    let package_managers = if args.packagemanagers.is_empty() {
        // Detect the installed package manager
        let package_managers = detect_package_managers(); // Get all installed packages
        if package_managers.is_empty() {
            if !args.minimal {
                println!("No supported package manager found");
            }
            return Ok(());
        }
        package_managers
    } else {
        // Find the package managers from sysargs
        let mut package_managers = vec![];
        for nme in args.packagemanagers {
            match match_package_managers(&nme) {
                Some(pm) => {
                    if pm.is_installed().unwrap() {
                        package_managers.push(pm);
                    } else if !args.minimal {
                        println!("Package manager {nme} not installed");
                    }
                }
                None => {
                    if !args.minimal {
                        println!("{nme} is not a supported package manager")
                    }
                }
            }
        }
        package_managers
    };
    let mut list = Vec::new();
    let mut readmes = Vec::new();
    for pm in package_managers {
        list.append(&mut pm.get_list_of_installed_packages().unwrap());
        // Get all READMEs
        readmes.append(&mut pm.get_all_readmes().unwrap());
    }
    if !args.minimal {
        println!(
            "Found {} packages (including duplicates) and a total of {} READMEs",
            list.len(),
            readmes.len()
        );
    }
    // Create a store. This couples READMEs to the packages as well.
    let store = create_store(list, readmes);
    // Vectors for collecting results and exceptions
    let mut exceptions = vec![];
    let mut matches = vec![];
    // For progress information
    let total_packages = store.len();
    // Loop over each package and find donation URLs in their READMEs
    for (i, inf) in store.values().sorted_by_key(|p| &p.name).enumerate() {
        match inf.find_donation_urls() {
            Ok(Some(url)) => matches.push((inf.name.clone(), url)),
            Err(err) => exceptions.push((inf.readme_path.clone().unwrap(), err)),
            _ => (),
        }
        if !args.minimal && i % args.notifyincrements == 0 {
            println!(
                "Searched in {} out of {} unique packages.",
                i, total_packages
            );
        }
    }
    // Print results
    if !args.minimal {
        println!("\nThe following donation URLs have been found in package READMEs:");
    }
    for (nme, urls) in matches {
        println!("{}: {}", nme, urls.join(", "))
    }
    if args.exceptions {
        println!("\nEXCEPTIONS");
        for (nme, err) in exceptions {
            println!("{}: {}", nme, err)
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_liberapay_regex() {
        let regex = Regex::new(LIBERAPAY_REGEX).unwrap();
        let protocols = ["http://", "https://", ""];
        let prefices = ["www.", ""];
        let base = "liberapay.com/foobar";
        let suffix = ["", "/", "/donate"];
        for p in protocols {
            for pre in prefices {
                for s in suffix {
                    let url = [p, pre, base, s].concat();
                    assert!(regex.is_match(&url))
                }
            }
        }
    }

    #[test]
    fn test_patreon_regex() {
        let regex = Regex::new(PATREON_REGEX).unwrap();
        let protocols = ["http://", "https://", ""];
        let prefices = ["www.", ""];
        let base = "patreon.com/foobar";
        for p in protocols {
            for pre in prefices {
                let url = [p, pre, base].concat();
                assert!(regex.is_match(&url))
            }
        }
    }
}
