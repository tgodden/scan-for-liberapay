A small program that searches your system for donation links to packages you use.

The program searches all README files of installed packages for donation links.

## Supported package managers:
- RPM
- pacman
- dpkg

## Supported donation platforms
- [Liberapay](https://www.liberapay.org)
- [Patreon](https://www.patreon.com)
